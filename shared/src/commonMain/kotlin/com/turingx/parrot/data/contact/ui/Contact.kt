package com.turingx.parrot.data.contact.ui

import kotlinx.serialization.Serializable

@Serializable
data class Contact(
    var id: String = "",
    var name: String = "",
    var photo: String? = null,
    var email: String = "",
    var phone: String = ""
) : IContact {

    companion object {

        const val CONTACT = "contact"

        fun toJson(contact: Contact): String {
            // TODO
            return ""
        }

        fun fromJson(contact: String) : Contact {
            // TODO
            return Contact()
        }

    }

    override fun isHeader() = false
}