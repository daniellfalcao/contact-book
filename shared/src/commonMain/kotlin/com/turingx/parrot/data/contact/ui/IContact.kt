package com.turingx.parrot.data.contact.ui

interface IContact {
    fun isHeader(): Boolean
}