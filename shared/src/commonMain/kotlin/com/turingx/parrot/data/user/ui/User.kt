package com.turingx.parrot.data.user.ui

data class User(
    val id: String,
    val name: String,
    val photo: String?,
    val email: String
)