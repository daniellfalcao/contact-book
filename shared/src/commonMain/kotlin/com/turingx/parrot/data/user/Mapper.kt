package com.turingx.parrot.data.user

import com.turingx.parrot.data.user.dto.UserDTO
import com.turingx.parrot.data.user.ui.User
import com.turingx.parrot.database.user.UserEntity

fun UserEntity.toDTO() = UserDTO(
    id = this.id,
    name = this.name,
    photo = this.photo,
    email = this.email
)

fun UserEntity.toUI() = User(
    id = this.id,
    name = this.name,
    photo = this.photo,
    email = this.email
)

fun UserDTO.toEntity() = UserEntity(
    id = this.id,
    name = this.name ?: "",
    photo = this.photo,
    email = this.email ?: ""
)

fun UserDTO.toUI() = User(
    id = this.id,
    name = this.name ?: "",
    photo = this.photo,
    email = this.email ?: ""
)