package com.turingx.parrot.data.user.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    @SerialName("_id") val id: String,
    @SerialName("name") val name: String?,
    @SerialName("photo") val photo: String?,
    @SerialName("email") val email: String?
)