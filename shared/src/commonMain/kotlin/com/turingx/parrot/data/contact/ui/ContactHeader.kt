package com.turingx.parrot.data.contact.ui

data class ContactHeader(val letter: String): IContact {
    override fun isHeader() = true
}