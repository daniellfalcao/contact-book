package com.turingx.parrot.data.contact.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ContactDTO(
    @SerialName("_id") val id: String,
    @SerialName("name") val name: String?,
    @SerialName("photo") val photo: String?,
    @SerialName("email") val email: String?,
    @SerialName("phone") val phone: String?
)