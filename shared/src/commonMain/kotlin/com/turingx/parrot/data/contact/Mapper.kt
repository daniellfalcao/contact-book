package com.turingx.parrot.data.contact

import com.turingx.parrot.data.contact.dto.ContactDTO
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.database.contact.ContactEntity

fun ContactEntity.toDTO() = ContactDTO(
    id = this.id,
    name = this.name,
    photo = this.photo,
    email = this.email,
    phone = this.phone
)

fun ContactEntity.toUI() = Contact(
    id = this.id,
    name = this.name,
    photo = this.photo,
    email = this.email,
    phone = this.phone
)

fun ContactDTO.toEntity() = ContactEntity(
    id = this.id,
    name = this.name ?: "",
    photo = this.photo,
    email = this.email ?: "",
    phone = this.phone ?: ""
)

fun ContactDTO.toUI() = Contact(
    id = this.id,
    name = this.name ?: "",
    photo = this.photo,
    email = this.email ?: "",
    phone = this.phone ?: ""
)