package com.turingx.parrot.data.user.dto

import kotlinx.serialization.Serializable

@Serializable
data class UserLoginDTO(
    val user: UserDTO,
    val token: String
)