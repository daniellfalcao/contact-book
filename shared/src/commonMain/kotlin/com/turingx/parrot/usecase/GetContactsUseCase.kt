package com.turingx.parrot.usecase

import com.turingx.parrot.core.usecase.CBUseCase
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.core.utilities.map
import com.turingx.parrot.data.contact.toUI
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.repository.contact.ContactRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class GetContactsUseCase(
    private val contactRepository: ContactRepository
): CBUseCase.Completable<Unit, List<Contact>, Flow<List<Contact>>>() {

    override suspend fun liveResult(params: Unit?): Flow<List<Contact>> {
        return contactRepository.flowContacts().map { contacts -> contacts.map { it.toUI() } }
    }

    override suspend fun execute(params: Unit?): CBResult<List<Contact>> {
        return contactRepository.fetchContacts().map { contacts -> contacts.map { it.toUI() } }
    }
}