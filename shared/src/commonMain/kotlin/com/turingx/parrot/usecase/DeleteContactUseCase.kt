package com.turingx.parrot.usecase

import com.turingx.parrot.core.usecase.CBUseCase
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.core.utilities.map
import com.turingx.parrot.data.contact.dto.ContactDTO
import com.turingx.parrot.data.contact.toUI
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.repository.contact.ContactRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DeleteContactUseCase(
    private val contactRepository: ContactRepository
): CBUseCase.Completable<String, Any, Flow<Contact>>() {

    override suspend fun liveResult(params: String?): Flow<Contact> {
        return contactRepository.flowContact(params ?: "").map { it.toUI() }
    }

    override suspend fun execute(params: String?): CBResult<Any> {
        return contactRepository.deleteContact(params ?: "")
    }
}