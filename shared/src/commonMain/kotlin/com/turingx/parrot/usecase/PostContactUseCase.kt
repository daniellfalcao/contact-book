package com.turingx.parrot.usecase

import com.turingx.parrot.core.usecase.CBUseCase
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.core.utilities.map
import com.turingx.parrot.data.contact.dto.ContactDTO
import com.turingx.parrot.data.contact.toUI
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.repository.contact.ContactRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class PostContactUseCase(
    private val contactRepository: ContactRepository
): CBUseCase.Completable<ContactDTO, Contact, Flow<Contact>>() {

    override suspend fun liveResult(params: ContactDTO?): Flow<Contact> {
        return contactRepository.flowContact(params?.id ?: "").map { it.toUI() }
    }

    override suspend fun execute(params: ContactDTO?): CBResult<Contact> {
        val contact = params ?: ContactDTO("", "", "", "", "")
        return contactRepository.createContact(contact).map { it.toUI() }
    }
}