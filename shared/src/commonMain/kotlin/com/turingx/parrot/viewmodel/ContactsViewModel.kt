package com.turingx.parrot.viewmodel

import com.turingx.parrot.core.usecase.CBUseCase.Completable.State
import com.turingx.parrot.core.usecase.CBUseCase.Completable.State.Idle
import com.turingx.parrot.core.utilities.CFlow
import com.turingx.parrot.core.utilities.Event
import com.turingx.parrot.core.utilities.MediatorCFlow
import com.turingx.parrot.core.viewmodel.CBViewModel
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.usecase.GetContactsUseCase
import dev.icerock.moko.mvvm.livedata.MediatorLiveData
import dev.icerock.moko.mvvm.livedata.MutableLiveData
import dev.icerock.moko.mvvm.livedata.readOnly
import kotlinx.coroutines.launch

class ContactsViewModel(
    val getContactsUseCase: GetContactsUseCase
) : CBViewModel() {

    private val _contacts = MediatorCFlow<List<Contact>>().apply {
        viewModelScope.launch {
            emit(getContactsUseCase.liveResult(null))
        }
    }
    private val _viewState = MutableLiveData<State>(Idle)

    val onContactClicked = MutableLiveData<Event.Data<Contact>?>(null)

    fun loadContacts() = viewModelScope.launch {

        getContactsUseCase().onStarted {

            _viewState.value = State.Executing

        }.onSuccess {

            _viewState.value = State.Success(it.isNotEmpty())

        }.onFailure {

            _viewState.value = State.Error.Generic(it)

        }.execute()
    }
}