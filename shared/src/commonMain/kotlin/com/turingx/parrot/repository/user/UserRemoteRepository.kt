package com.turingx.parrot.repository.user

import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.data.user.dto.UserDTO
import com.turingx.parrot.data.user.dto.UserLoginDTO

class UserRemoteRepository : CBRepository.Remote() {

    private val api = UserAPI(client)

    suspend fun login(email: String, password: String): CBResult<UserLoginDTO> {
        return executeRequest(api) { login(email, password) }
    }

    suspend fun logout(token: String): CBResult<Any> {
        return executeRequest(api) { logout(token) }
    }

    suspend fun register(user: UserDTO): CBResult<UserLoginDTO> {
        return executeRequest(api) { register(user) }
    }
}