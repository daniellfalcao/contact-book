package com.turingx.parrot.repository.token

import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.database.token.TokenEntity
import com.turingx.parrot.database.token.TokenEntityQueries

class TokenLocalRepository(private val tokenDAO: TokenEntityQueries) : CBRepository.Local() {

    suspend fun getToken(): String {
        return tokenDAO.get().executeAsOneOrNull()?.token ?: ""
    }

    suspend fun deleteToken() {
         tokenDAO.delete()
    }

    suspend fun saveToken(tokenEntity: TokenEntity) {
        tokenDAO.insert(tokenEntity)
    }
}