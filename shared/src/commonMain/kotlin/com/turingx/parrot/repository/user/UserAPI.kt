package com.turingx.parrot.repository.user

import com.turingx.parrot.core.repository.CBApi
import com.turingx.parrot.core.repository.CBRepository.Remote.Companion.API_TOKEN
import com.turingx.parrot.data.user.dto.UserDTO
import com.turingx.parrot.data.user.dto.UserLoginDTO
import io.ktor.client.HttpClient
import io.ktor.client.request.delete
import io.ktor.client.request.header
import io.ktor.client.request.post

class UserAPI(client: HttpClient): CBApi(client) {

    suspend fun login(email: String, password: String): UserLoginDTO {
        return client.post("http://contatosapi.herokuapp.com/api/login") {
            body = mapOf("email" to email, "password" to password)
        }
    }

    suspend fun logout(token: String): String {
        return client.delete("http://contatosapi.herokuapp.com/api/logout") {
            header(API_TOKEN, token)
        }
    }

    suspend fun register(user: UserDTO): UserLoginDTO {
        return client.post("http://contatosapi.herokuapp.com/api/signup") {
            body = user
        }
    }
}