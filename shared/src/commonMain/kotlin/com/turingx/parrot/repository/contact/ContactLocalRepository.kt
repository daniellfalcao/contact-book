package com.turingx.parrot.repository.contact

import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.turingx.parrot.core.coroutine.CBDispatchers
import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.data.contact.dto.ContactDTO
import com.turingx.parrot.database.contact.ContactEntity
import com.turingx.parrot.database.contact.ContactEntityQueries
import kotlinx.coroutines.flow.Flow

class ContactLocalRepository(private val contactDAO: ContactEntityQueries) : CBRepository.Local() {

    suspend fun getContact(id: String): ContactEntity? {
        return contactDAO.get(id).executeAsOneOrNull()
    }

    suspend fun flowContact(id: String): Flow<ContactEntity> {
        return contactDAO.get(id).asFlow().mapToOne(CBDispatchers.Default)
    }

    suspend fun getContacts(): List<ContactEntity> {
        return contactDAO.getAll().executeAsList()
    }

    suspend fun flowContacts(): Flow<List<ContactEntity>> {
        return contactDAO.getAll().asFlow().mapToList(CBDispatchers.Default)
    }

    fun saveContact(contactEntity: ContactEntity) {
        contactDAO.insert(contactEntity)
    }

    fun saveContacts(contacts: List<ContactEntity>) {
        try {
            database?.transaction { contacts.forEach { saveContact(it) } }
        } catch (e: Exception) {
            // tratar
        }
    }

    fun updateContact(contactEntity: ContactEntity) {
//        contactDAO.update(contactEntity)
    }

    fun deleteContact(contactId: String) {
        contactDAO.delete(contactId)
    }
}