package com.turingx.parrot.repository.contact

import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.data.contact.dto.ContactDTO

class ContactRemoteRepository : CBRepository.Remote() {

    private val api = ContactAPI(client)

    suspend fun createContact(contact: ContactDTO, token: String): CBResult<ContactDTO> {
        return executeRequest(api) { createContact(contact, token) }
    }

    suspend fun fetchContacts(token: String): CBResult<List<ContactDTO>> {
        return executeRequest(api) { fetchContacts(token) }
    }

    suspend fun fetchContact(contactId: String, token: String): CBResult<ContactDTO> {
        return executeRequest(api) { fetchContact(contactId, token) }
    }

    suspend fun updateContact(contact: ContactDTO, token: String): CBResult<ContactDTO> {
        return executeRequest(api) { updateContact(contact, token) }
    }

    suspend fun deleteContact(contactId: String, token: String): CBResult<Any> {
        return executeRequest(api) { deleteContact(contactId, token) }
    }

}