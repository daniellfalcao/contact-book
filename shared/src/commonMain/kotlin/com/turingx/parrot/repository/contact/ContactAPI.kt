package com.turingx.parrot.repository.contact

import com.turingx.parrot.core.repository.CBApi
import com.turingx.parrot.core.repository.CBRepository.Remote.Companion.API_TOKEN
import com.turingx.parrot.data.contact.dto.ContactDTO
import io.ktor.client.HttpClient
import io.ktor.client.request.*

class ContactAPI(client: HttpClient) : CBApi(client) {

    suspend fun createContact(contact: ContactDTO, token: String): ContactDTO {
        return client.post("http://contatosapi.herokuapp.com/api/contacts") {
            header(API_TOKEN, token)
            body = contact
        }
    }

    suspend fun fetchContacts(token: String): List<ContactDTO> {
        return client.get("http://contatosapi.herokuapp.com/api/contacts") {
            header(API_TOKEN, token)
        }
    }

    suspend fun fetchContact(contactId: String, token: String): ContactDTO {
        return client.get("http://contatosapi.herokuapp.com/api/contacts/$contactId") {
            header(API_TOKEN, token)
        }
    }

    suspend fun updateContact(contact: ContactDTO, token: String): ContactDTO {
        return client.put("http://contatosapi.herokuapp.com/api/contacts/${contact.id}") {
            header(API_TOKEN, token)
            body = contact
        }
    }

    suspend fun deleteContact(contactId: String, token: String): Any {
        return client.delete("http://contatosapi.herokuapp.com/api/contacts/$contactId") {
            header(API_TOKEN, token)
        }
    }

}