package com.turingx.parrot.repository.contact

import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.core.utilities.onSuccess
import com.turingx.parrot.data.contact.dto.ContactDTO
import com.turingx.parrot.data.contact.toEntity
import com.turingx.parrot.database.contact.ContactEntity
import com.turingx.parrot.repository.token.TokenLocalRepository
import kotlinx.coroutines.flow.Flow


class ContactRepository(
    private val tokenLocalRepository: TokenLocalRepository,
    private val contactRemoteRepository: ContactRemoteRepository,
    private val contactLocalRepository: ContactLocalRepository
) : CBRepository() {

    suspend fun createContact(contact: ContactDTO): CBResult<ContactDTO> {
        val token = tokenLocalRepository.getToken() ?: ""
        return contactRemoteRepository.createContact(contact, token).onSuccess {
            contactLocalRepository.saveContact(it.toEntity())
        }
    }

    suspend fun fetchContacts(): CBResult<List<ContactDTO>> {
        val token = tokenLocalRepository.getToken()
        return contactRemoteRepository.fetchContacts(token).onSuccess { contacts ->
            contactLocalRepository.saveContacts(contacts.map { it.toEntity() })
        }
    }

    suspend fun fetchContact(contactId: String): CBResult<ContactDTO> {
        val token = tokenLocalRepository.getToken()
        return contactRemoteRepository.fetchContact(contactId, token).onSuccess {
            contactLocalRepository.saveContact(it.toEntity())
        }
    }

    suspend fun updateContact(contact: ContactDTO): CBResult<ContactDTO> {
        val token = tokenLocalRepository.getToken()
        return contactRemoteRepository.updateContact(contact, token).onSuccess {
            contactLocalRepository.updateContact(it.toEntity())
        }
    }

    suspend fun deleteContact(contactId: String): CBResult<Any> {
        val token = tokenLocalRepository.getToken()
        return contactRemoteRepository.deleteContact(contactId, token).onSuccess {
            contactLocalRepository.deleteContact(contactId)
        }
    }

    suspend fun getContact(id: String): ContactEntity? {
        return contactLocalRepository.getContact(id)
    }

    suspend fun flowContact(id: String): Flow<ContactEntity> {
        return contactLocalRepository.flowContact(id)
    }

    suspend fun getContacts(): List<ContactEntity> {
        return contactLocalRepository.getContacts()
    }

    suspend fun flowContacts(): Flow<List<ContactEntity>> {
        return contactLocalRepository.flowContacts()
    }
}