package com.turingx.parrot.repository.user

import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.core.utilities.mapFailure
import com.turingx.parrot.core.utilities.onSuccess
import com.turingx.parrot.data.user.dto.UserDTO
import com.turingx.parrot.data.user.dto.UserLoginDTO
import com.turingx.parrot.data.user.toEntity
import com.turingx.parrot.database.token.TokenEntity
import com.turingx.parrot.database.user.UserEntity
import com.turingx.parrot.repository.token.TokenLocalRepository

class UserRepository(
    private val userRemoteRepository: UserRemoteRepository,
    private val userLocalRepository: UserLocalRepository,
    private val tokenLocalRepository: TokenLocalRepository
) : CBRepository() {

    suspend fun login(email: String, password: String): CBResult<UserEntity> {
        return userRemoteRepository.login(email, password).onSuccess {
            createUserSession(it)
        }.mapFailure()
    }

    suspend fun logout(): CBResult<Any> {
        return userRemoteRepository.logout("").onSuccess {
            userLocalRepository.deleteUser()
        }
    }

    suspend fun register(user: UserDTO): CBResult<UserLoginDTO> {
        return userRemoteRepository.register(user).onSuccess {
            createUserSession(it)
        }
    }

    private suspend fun createUserSession(userWrapper: UserLoginDTO) {
        val user = userWrapper.user.toEntity()
        userLocalRepository.saveUser(user)
        tokenLocalRepository.saveToken(TokenEntity("0", userWrapper.token, user.id))
    }
}