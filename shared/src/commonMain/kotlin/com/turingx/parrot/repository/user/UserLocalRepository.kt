package com.turingx.parrot.repository.user

import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.turingx.parrot.core.coroutine.CBDispatchers
import com.turingx.parrot.core.repository.CBRepository
import com.turingx.parrot.database.user.UserEntity
import com.turingx.parrot.database.user.UserEntityQueries
import kotlinx.coroutines.flow.Flow

class UserLocalRepository(private val userDAO: UserEntityQueries) : CBRepository.Local() {

    suspend fun getUser(): UserEntity? {
        return userDAO.get().executeAsOneOrNull()
    }

    suspend fun flowUser(): Flow<UserEntity> {
        return userDAO.get().asFlow().mapToOne(CBDispatchers.Default)
    }

    suspend fun saveUser(user: UserEntity) {
        userDAO.insert(user)
    }

    suspend fun deleteUser() {
        userDAO.delete()
    }

}