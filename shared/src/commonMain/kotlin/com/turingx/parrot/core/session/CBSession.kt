package com.turingx.parrot.core.session

import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.turingx.parrot.core.database.CBDatabase
import com.turingx.parrot.data.user.toUI
import com.turingx.parrot.data.user.ui.User
import kotlinx.coroutines.flow.*

object CBSession {

    val user: Flow<User> = CBDatabase.instance?.userEntityQueries?.get()?.asFlow()?.mapToOne()?.map { it.toUI() } ?: flowOf()
    val token: String = CBDatabase.instance?.tokenEntityQueries?.get()?.executeAsOneOrNull()?.token ?: ""

    fun isAuthenticated() = token.isNotBlank()

}