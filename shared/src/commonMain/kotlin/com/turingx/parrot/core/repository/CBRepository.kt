package com.turingx.parrot.core.repository

import com.turingx.parrot.core.database.CBDatabase
import com.turingx.parrot.core.network.CBClient
import com.turingx.parrot.core.network.toNetworkException
import com.turingx.parrot.core.utilities.CBResult

open class CBRepository {

    open class Local {
        protected val database = CBDatabase.instance
    }

    open class Remote {

        companion object {
            const val API_TOKEN = "token"
        }

        val client = CBClient().client

        suspend fun <API, T> executeRequest(api: API, request: suspend API.() -> T): CBResult<T> {
            return try {
                CBResult(api.request())
            } catch (e: Exception) {
                CBResult.failure(e.toNetworkException())
            }
        }
    }

}