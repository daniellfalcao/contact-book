package com.turingx.parrot.core.database

import com.squareup.sqldelight.db.SqlDriver

expect class CBDatabaseDriver {
    val driver: SqlDriver
}