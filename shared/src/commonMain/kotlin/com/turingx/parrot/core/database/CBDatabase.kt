package com.turingx.parrot.core.database

import com.turingx.parrot.database.ContactBookDatabase

const val CB_DATABASE = "contact_book.db"

expect class CBDatabase private constructor() {

    companion object {
        internal var instance: ContactBookDatabase?
    }
}