package com.turingx.parrot.core.utilities

import com.turingx.parrot.core.coroutine.CBDispatchers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

abstract class CFlow<T> {

    private val job = Job()
    private val scope = CoroutineScope(CBDispatchers.Main + job)

    private var _watch: (T) -> Unit = { }
    protected var flow: Flow<T>? = null

    fun watch(block: (T) -> Unit) {
        _watch = block
    }

    protected fun startWatchFlow() {
        flow?.onEach { _watch(it) }?.launchIn(scope)
    }
}