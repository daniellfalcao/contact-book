package com.turingx.parrot.core.network

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

class CBClient {

    val client = HttpClient(CBEngine().engine) {
        install(JsonFeature) {
            serializer = KotlinxSerializer(Json(JsonConfiguration.Default))
        }
    }
}