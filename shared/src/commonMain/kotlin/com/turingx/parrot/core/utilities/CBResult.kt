package com.turingx.parrot.core.utilities

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * A discriminated union that encapsulates successful outcome with a value of type [T]
 * or a failure with an arbitrary [Throwable] exception.
 */
@Suppress("NON__PRIMARY_CONSTRUCTOR_OF__CLASS", "UNCHECKED_CAST")
open class CBResult<out T>(val value: Any?) {

    /**
     * Returns `true` if this instance represents successful outcome.
     * In this case [isFailure] returns `false`.
     */
    val isSuccess: Boolean get() = value !is Failure

    /**
     * Returns `true` if this instance represents failed outcome.
     * In this case [isSuccess] returns `false`.
     */
    val isFailure: Boolean get() = value is Failure

    // value & exception retrieval

    /**
     * Returns the encapsulated value if this instance represents [success][CBResult.isSuccess] or `null`
     * if it is [failure][CBResult.isFailure].
     *
     * This function is shorthand for `getOrElse { null }` (see [getOrElse]) or
     * `fold(onSuccess = { it }, onFailure = { null })` (see [fold]).
     */
    fun getOrNull(): T? =
        when {
            isFailure -> null
            else -> value as T
        }

    /**
     * Returns the encapsulated exception if this instance represents [failure][isFailure] or `null`
     * if it is [success][isSuccess].
     *
     * This function is shorthand for `fold(onSuccess = { null }, onFailure = { it })` (see [fold]).
     */
    fun exceptionOrNull(): Throwable? =
        when (value) {
            is Failure -> value.exception
            else -> null
        }

    /**
     * Returns a string `Success(v)` if this instance represents [success][CBResult.isSuccess]
     * where `v` is a string representation of the value or a string `Failure(x)` if
     * it is [failure][isFailure] where `x` is a string representation of the exception.
     */
    override fun toString(): String =
        when (value) {
            is Failure -> value.toString() // "Failure($exception)"
            else -> "Success($value)"
        }

    // companion with constructors

    /**
     * Companion object for [CBResult] class that contains its constructor functions
     * [success] and [failure].
     */
    companion object {
        /**
         * Returns an instance that encapsulates the given [value] as successful value.
         */

        fun <T> success(value: T): CBResult<T> =
            CBResult(value)

        /**
         * Returns an instance that encapsulates the given [exception] as failure.
         */

        fun <T> failure(exception: Throwable): CBResult<T> =
            CBResult(
                createFailure(
                    exception
                )
            )
    }

    class Failure(val exception: Throwable) {
        override fun equals(other: Any?): Boolean = other is Failure && exception == other.exception
        override fun hashCode(): Int = exception.hashCode()
        override fun toString(): String = "Failure($exception)"
    }
}

/**
 * Creates an instance of internal marker [CBResult.Failure] class to
 * make sure that this class is not exposed in ABI.
 */
internal fun createFailure(exception: Throwable): Any =
    CBResult.Failure(exception)

/**
 * Throws exception if the NewResult is failure. This internal function minimizes
 * d bytecode for [getOrThrow] and makes sure that in the future we can
 * add some exception-augmenting logic here (if needed).
 */
fun CBResult<*>.throwOnFailure() {
    if (value is CBResult.Failure) throw value.exception
}

/**
 * Calls the specified function [block] and returns its encapsulated NewResult if invocation was successful,
 * catching and encapsulating any thrown exception as a failure.
 */
fun <R> runCatching(block: () -> R): CBResult<R> {
    return try {
        CBResult.success(block())
    } catch (e: Throwable) {
        CBResult.failure(e)
    }
}

/**
 * Calls the specified function [block] with `this` value as its receiver and returns its encapsulated NewResult
 * if invocation was successful, catching and encapsulating any thrown exception as a failure.
 */
fun <T, R> T.runCatching(block: T.() -> R): CBResult<R> {
    return try {
        CBResult.success(block())
    } catch (e: Throwable) {
        CBResult.failure(e)
    }
}

// -- extensions ---

/**
 * Returns the encapsulated value if this instance represents [success][CBResult.isSuccess] or throws the encapsulated exception
 * if it is [failure][CBResult.isFailure].
 *
 * This function is shorthand for `getOrElse { throw it }` (see [getOrElse]).
 */
@Suppress("UNCHECKED_CAST")
fun <T> CBResult<T>.getOrThrow(): T {
    throwOnFailure()
    return value as T
}

/**
 * Returns the encapsulated value if this instance represents [success][CBResult.isSuccess] or the
 * NewResult of [onFailure] function for encapsulated exception if it is [failure][CBResult.isFailure].
 *
 * Note, that an exception thrown by [onFailure] function is rethrown by this function.
 *
 * This function is shorthand for `fold(onSuccess = { it }, onFailure = onFailure)` (see [fold]).
 */

@Suppress("UNCHECKED_CAST")
@ExperimentalContracts
fun <R, T : R> CBResult<T>.getOrElse(onFailure: (exception: Throwable) -> R): R {
    contract {
        callsInPlace(onFailure, InvocationKind.AT_MOST_ONCE)
    }
    return when (val exception = exceptionOrNull()) {
        null -> value as T
        else -> onFailure(exception)
    }
}

/**
 * Returns the encapsulated value if this instance represents [success][CBResult.isSuccess] or the
 * [defaultValue] if it is [failure][CBResult.isFailure].
 *
 * This function is shorthand for `getOrElse { defaultValue }` (see [getOrElse]).
 */

@Suppress("UNCHECKED_CAST")
fun <R, T : R> CBResult<T>.getOrDefault(defaultValue: R): R {
    if (isFailure) return defaultValue
    return value as T
}

/**
 * Returns the the NewResult of [onSuccess] for encapsulated value if this instance represents [success][CBResult.isSuccess]
 * or the NewResult of [onFailure] function for encapsulated exception if it is [failure][CBResult.isFailure].
 *
 * Note, that an exception thrown by [onSuccess] or by [onFailure] function is rethrown by this function.
 */

@Suppress("UNCHECKED_CAST")
@ExperimentalContracts
fun <R, T> CBResult<T>.fold(
    onSuccess: (value: T) -> R,
    onFailure: (exception: Throwable) -> R
): R {
    contract {
        callsInPlace(onSuccess, InvocationKind.AT_MOST_ONCE)
        callsInPlace(onFailure, InvocationKind.AT_MOST_ONCE)
    }
    return when (val exception = exceptionOrNull()) {
        null -> onSuccess(value as T)
        else -> onFailure(exception)
    }
}

// transformation

/**
 * Returns the encapsulated NewResult of the given [transform] function applied to encapsulated value
 * if this instance represents [success][CBResult.isSuccess] or the
 * original encapsulated exception if it is [failure][CBResult.isFailure].
 *
 * Note, that an exception thrown by [transform] function is rethrown by this function.
 * See [mapCatching] for an alternative that encapsulates exceptions.
 */
@Suppress("UNCHECKED_CAST")
fun <R, T> CBResult<T>.map(transform: (value: T) -> R): CBResult<R> {
    return when {
        isSuccess -> CBResult.success(transform(value as T))
        else -> CBResult(value)
    }
}

/**
 * Returns the encapsulated NewResult of the given [transform] function applied to encapsulated value
 * if this instance represents [success][CBResult.isSuccess] or the
 * original encapsulated exception if it is [failure][CBResult.isFailure].
 *
 * Any exception thrown by [transform] function is caught, encapsulated as a failure and returned by this function.
 * See [map] for an alternative that rethrows exceptions.
 */
@Suppress("UNCHECKED_CAST")
fun <R, T> CBResult<T>.mapCatching(transform: (value: T) -> R): CBResult<R> {
    return when {
        isSuccess -> runCatching { transform(value as T) }
        else -> CBResult(value)
    }
}

fun <R> CBResult<*>.mapFailure(): CBResult<R> {
    return this as CBResult<R>
}

/**
 * Returns the encapsulated NewResult of the given [transform] function applied to encapsulated exception
 * if this instance represents [failure][CBResult.isFailure] or the
 * original encapsulated value if it is [success][CBResult.isSuccess].
 *
 * Note, that an exception thrown by [transform] function is rethrown by this function.
 * See [recoverCatching] for an alternative that encapsulates exceptions.
 */

@ExperimentalContracts
fun <R, T : R> CBResult<T>.recover(transform: (exception: Throwable) -> R): CBResult<R> {
    contract {
        callsInPlace(transform, InvocationKind.AT_MOST_ONCE)
    }
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> CBResult.success(transform(exception))
    }
}

/**
 * Returns the encapsulated NewResult of the given [transform] function applied to encapsulated exception
 * if this instance represents [failure][CBResult.isFailure] or the
 * original encapsulated value if it is [success][CBResult.isSuccess].
 *
 * Any exception thrown by [transform] function is caught, encapsulated as a failure and returned by this function.
 * See [recover] for an alternative that rethrows exceptions.
 */

fun <R, T : R> CBResult<T>.recoverCatching(transform: (exception: Throwable) -> R): CBResult<R> {
    val value = value // workaround for  classes BE bug
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> runCatching { transform(exception) }
    }
}

// "peek" onto value/exception and pipe

/**
 * Performs the given [action] on encapsulated exception if this instance represents [failure][CBResult.isFailure].
 * Returns the original `NewResult` unchanged.
 */

suspend fun <T> CBResult<T>.onFailure(action: suspend (exception: Throwable) -> Unit): CBResult<T> {
    exceptionOrNull()?.let { action(it) }
    return this
}

/**
 * Performs the given [action] on encapsulated value if this instance represents [success][CBResult.isSuccess].
 * Returns the original `NewResult` unchanged.
 */
@Suppress("UNCHECKED_CAST")
suspend fun <T> CBResult<T>.onSuccess(action: suspend (value: T) -> Unit): CBResult<T> {
    if (isSuccess) action(value as T)
    return this
}