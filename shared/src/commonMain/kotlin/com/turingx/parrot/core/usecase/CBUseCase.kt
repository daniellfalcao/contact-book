package com.turingx.parrot.core.usecase

import com.turingx.parrot.core.coroutine.CBDispatchers
import com.turingx.parrot.core.exception.CBException
import com.turingx.parrot.core.utilities.CBResult
import com.turingx.parrot.core.utilities.onFailure
import com.turingx.parrot.core.utilities.onSuccess
import kotlinx.coroutines.*

interface ICBUseCase<PARAMS, RESULT, LIVERESULT> {
    suspend fun liveResult(params: PARAMS? = null): LIVERESULT
    suspend fun execute(params: PARAMS? = null): CBResult<RESULT>
}

abstract class CBUseCase<PARAMS, RESULT, out EXECUTOR : CBUseCase.UseCaseExecutor<PARAMS, RESULT>, LIVERESULT> private constructor() : ICBUseCase<PARAMS, RESULT, LIVERESULT> {

    abstract class UseCaseExecutor<PARAMS, RESULT>(var params: PARAMS? = null): CoroutineScope by MainScope() {
        abstract suspend fun execute()
    }

    abstract class Completable<PARAMS, RESULT, LIVERESULT> : CBUseCase<PARAMS, RESULT, Completable<PARAMS, RESULT, LIVERESULT>.CompletableUseCaseExecutor, LIVERESULT>() {

        inner class CompletableUseCaseExecutor : UseCaseExecutor<PARAMS, RESULT>() {

            private var _onStarted = { }
            private var _onSuccess: (RESULT) -> Unit = { }
            private var _onFailure: (CBException) -> Unit = { }
            private var _onFinish = { }

            fun onStarted(callback: () -> Unit) = apply { _onStarted = callback }

            fun onSuccess(callback: (RESULT) -> Unit) = apply { _onSuccess = callback }

            fun onFailure(callback: (CBException) -> Unit) = apply { _onFailure = callback }

            fun onFinish(callback: () -> Unit) = apply { _onFinish = callback }

            override suspend fun execute() {
                _onStarted()
                try {
                    withContext(CBDispatchers.Default) { execute(params) }.onSuccess {
                        withContext(Dispatchers.Main) { _onSuccess(it) }
                    }.onFailure {
                        withContext(Dispatchers.Main) { if (it is CBException) _onFailure(it) else _onFailure(CBException(cause = it)) }
                    }
                }catch (e: Exception) {
                    if (e is CBException) _onFailure(e) else _onFailure(CBException(cause = e))
                }
                withContext(Dispatchers.Main) { _onFinish() }
            }
        }

        sealed class State {
            object Idle : State()
            object Executing : State()
            class Success(val hasContent: Boolean) : State()
            sealed class Error(val exception: CBException) : State() {
                class Connection(exception: CBException) : Error(exception)
                class Generic(exception: CBException) : Error(exception)
            }
        }

        override val useCaseExecutor = CompletableUseCaseExecutor()
    }

    abstract val useCaseExecutor: EXECUTOR

    override suspend fun liveResult(params: PARAMS?): LIVERESULT {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun execute(params: PARAMS?): CBResult<RESULT> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    operator fun invoke(params: PARAMS? = null): EXECUTOR {
        return useCaseExecutor.apply { this.params = params }
    }
}