package com.turingx.parrot.core.utilities

import dev.icerock.moko.mvvm.livedata.LiveData
import dev.icerock.moko.mvvm.livedata.MutableLiveData

abstract class Event<T> {

    protected open var hasBeenHandled = false
    protected open var content: T? = null

    open fun getContentIfNotHandled(): T? {
        return if (!hasBeenHandled) {
            hasBeenHandled = true
            content
        } else {
            null
        }
    }

    open fun peekContent(): T? = content

    class Data<T>(override var content: T?) : Event<T>()

    class Callback : Event<Any>() {
        override var content: Any? = "CALLBACK"
    }
}

fun <T> LiveData<Event.Data<T>>.observeEvent(observer: (T) -> Unit) {
    addObserver { event -> event.getContentIfNotHandled()?.let { observer(it) } }
}

fun LiveData<Event.Callback>.observeEvent(observer: () -> Unit) {
    addObserver { event -> event.getContentIfNotHandled()?.let { observer() } }
}

fun MutableLiveData<Event.Callback>.call() {
    this.value = Event.Callback()
}
