package com.turingx.parrot.core.coroutine

import kotlinx.coroutines.CoroutineDispatcher

expect object CBDispatchers {
    val Main: CoroutineDispatcher
    val Default: CoroutineDispatcher
    val Io: CoroutineDispatcher
}