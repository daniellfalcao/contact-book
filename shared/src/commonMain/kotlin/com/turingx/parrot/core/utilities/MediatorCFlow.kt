package com.turingx.parrot.core.utilities

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach

class MediatorCFlow<T> : CFlow<T>() {

    fun emit(flow: Flow<T>) {
        this.flow?.onEach { }
        this.flow = flow
        startWatchFlow()
    }
}