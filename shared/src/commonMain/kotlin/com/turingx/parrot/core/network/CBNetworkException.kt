package com.turingx.parrot.core.network

import com.turingx.parrot.core.exception.CBException
import io.ktor.client.features.ClientRequestException

fun Throwable.toNetworkException() = CBNetworkException(this)

class CBNetworkException(throwable: Throwable) : CBException(cause = throwable) {

    val errorCode: NetworkExceptionCode = when (cause) {
        is ClientRequestException -> {
            when (cause.response.status.value) {
                in 400..499 -> NetworkExceptionCode.CLIENT
                in 500..599 -> NetworkExceptionCode.SERVER
                else -> NetworkExceptionCode.UNEXPECTED
            }
        }
        else -> NetworkExceptionCode.UNEXPECTED
    }

    override val message: String? = ""

    init {
        println("error -> ${cause.toString()}")
    }

}

/**
 *  Códigos de erro:
 *
 *  @CLIENT - Um erro na faixa de 400 .. 499
 *  @SERVER - Um erro na faixa de 500 .. 599
 *
 *  @BAD_REQUEST/400 - O request é invalido ou não pode ser servido. Geralmente o JSON pode não serválido.
 *  @UNAUTHORIZED/401 - A requisição requer autenticação do usuário.
 *  @FORBIDDEN/403 - O servidor entende a requisição mas o acesso não está liberado.
 *  @NOT_FOUND/404 - Não foi encontrado o que se procura.
 *
 *  @TIMEOUT - Esgotou o tempo limite da requisição.
 *  @UNKNOWN_HOST - O endereço IP do host não foi encontrado, acontece também quando não está conectado a internet.
 *  @CONNECTION - Sem conexão com a internet.
 *  @CANCELED - A requisição foi cancelada, normalmente o usuário recebeu UNAUTHORIZED em outra requisição paralela a essa.
 *
 *  @UNEXPECTED - Um erro inesperado.
 *
 * */
enum class NetworkExceptionCode(val message: String) {
    CLIENT("Tente novamente"),
    SERVER("Serviço indisponível"),
    BAD_REQUEST("Tente novamente"),
    UNAUTHORIZED("Usuário não autorizado"),
    FORBIDDEN("Tente novamente"),
    NOT_FOUND("Não encontrado"),
    TIMEOUT("Tente novamente"),
    UNKNOWN_HOST("Não foi possível conectar. Verifique sua conexão e tente novamente."),
    CONNECTION("Não foi possível conectar. Verifique sua conexão e tente novamente."),
    CANCELED("Tente novamente"),
    UNEXPECTED("Tente novamente")
}