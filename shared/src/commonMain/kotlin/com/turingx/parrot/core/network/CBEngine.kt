package com.turingx.parrot.core.network

import io.ktor.client.engine.HttpClientEngine

expect class CBEngine() {
    val engine: HttpClientEngine
}