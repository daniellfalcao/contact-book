package com.turingx.parrot.core.database

import android.content.Context
import com.turingx.parrot.database.ContactBookDatabase

actual class CBDatabase private actual constructor() {

    actual companion object {

        internal actual var instance: ContactBookDatabase? = null

        fun initialize(context: Context): ContactBookDatabase {
            return if (instance != null) instance!!
            else ContactBookDatabase(CBDatabaseDriver(context, CB_DATABASE).driver)
        }

    }
}