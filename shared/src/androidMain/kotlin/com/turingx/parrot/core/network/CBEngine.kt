package com.turingx.parrot.core.network

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.android.Android

actual class CBEngine {
    actual val engine: HttpClientEngine = Android.create()
}