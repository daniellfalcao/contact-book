package com.turingx.parrot.core.database

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import com.turingx.parrot.database.ContactBookDatabase

actual class CBDatabaseDriver(context: Context, databaseName: String) {
    actual val driver: SqlDriver = AndroidSqliteDriver(ContactBookDatabase.Schema, context, databaseName)
}