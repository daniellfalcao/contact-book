package com.turingx.parrot.core.coroutine

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import platform.darwin.dispatch_async
import platform.darwin.dispatch_get_main_queue
import kotlin.coroutines.CoroutineContext

actual object CBDispatchers {

    actual val Main: CoroutineDispatcher = iOSDispatcher
    actual val Default: CoroutineDispatcher = iOSDispatcher
    actual val Io: CoroutineDispatcher = iOSDispatcher

    @Suppress("ClassName")
    object iOSDispatcher : CoroutineDispatcher() {
        override fun dispatch(context: CoroutineContext, block: Runnable) {
            dispatch_async(dispatch_get_main_queue()) { block.run() }
        }
    }
}