package com.turingx.parrot.core.database

import com.turingx.parrot.database.ContactBookDatabase

actual class CBDatabase private actual constructor() {

    actual companion object {

        internal actual var instance: ContactBookDatabase? = null

        fun initialize(): ContactBookDatabase {
            return if (instance != null) instance!!
            else ContactBookDatabase(CBDatabaseDriver(CB_DATABASE).driver)
        }
    }

}