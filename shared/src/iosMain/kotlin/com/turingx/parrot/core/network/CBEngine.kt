package com.turingx.parrot.core.network

import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.ios.Ios

actual class CBEngine {
    actual val engine: HttpClientEngine = Ios.create()
}