package com.turingx.parrot.core.database

import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver
import com.turingx.parrot.database.ContactBookDatabase

actual class CBDatabaseDriver(databaseName: String) {
    actual val driver: SqlDriver = NativeSqliteDriver(ContactBookDatabase.Schema, databaseName)
}