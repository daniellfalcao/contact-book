import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

val frameworkName: String by extra

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    kotlin("native.cocoapods")
    id("com.android.library")
    id("kotlin-android-extensions")
    id("com.squareup.sqldelight")
}

group = "com.turingx.parrot"
version = "1.0.0"

repositories {
    gradlePluginPortal()
    google()
    jcenter()
    mavenCentral()
    maven { url = uri("https://dl.bintray.com/icerockdev/moko") }
}

sqldelight {
    database("ContactBookDatabase") { // This will be the name of the generated database class.
        packageName = "com.turingx.parrot.database"
    }
}

kotlin {

    // configure ios target
    val iOSTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget =
        if (System.getenv("SDK_NAME")?.startsWith("iphoneos") == true)
            ::iosArm64
        else
            ::iosX64

    iOSTarget("ios") {
        compilations {
            val main by getting {
                kotlinOptions.freeCompilerArgs = listOf("-Xobjc-generics")
            }
        }
    }

    // configure android target
    android()

    // configure cocoapods libs
    cocoapods {
        summary = "CocoaPods shared library"
        homepage = "https://github.com/JetBrains/kotlin"
//        pod("AFNetworking", "~> 4.0.0")
//        pod("SDWebImage/MapKit")
    }

    // configure kotlin mpp libs
    sourceSets {

        // libs version
        val kotlinCoroutinesVersion: String by extra
        val kotlinSerializationVersion: String by extra
        val ktorVersion: String by extra
        val sqlDelightVersion: String by extra
        val mokoMvvmVersion: String by extra
        val androidxLifecycleVersion: String by extra

        val commonMain by getting {
            dependencies {
                // kotlin
                implementation(kotlin("stdlib-common"))

                // kotlin serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:$kotlinSerializationVersion")

                // kotlin coroutines
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$kotlinCoroutinesVersion")

                // ktor
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-json:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization:$ktorVersion")

                // sqlDelight
                implementation("com.squareup.sqldelight:runtime:$sqlDelightVersion")
                implementation("com.squareup.sqldelight:coroutines-extensions:$sqlDelightVersion")

                // moko mvvm
                api("dev.icerock.moko:mvvm:$mokoMvvmVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                // kotlin
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }

        val androidMain by getting {
            dependencies {
                // kotlin
                implementation(kotlin("stdlib"))
                implementation(kotlin("stdlib-jdk7"))

                // androidx lifecycle
                implementation("androidx.lifecycle:lifecycle-extensions:$androidxLifecycleVersion")

                // kotlin coroutines
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutinesVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$kotlinCoroutinesVersion")

                // ktor
                implementation("io.ktor:ktor-client-android:$ktorVersion")
                implementation("io.ktor:ktor-client-json-jvm:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization-jvm:$ktorVersion")

                // sqlDelight
                implementation("com.squareup.sqldelight:android-driver:$sqlDelightVersion")

            }
        }
        val androidTest by getting {

        }

        val iosMain by getting {
            dependencies {
                // kotlin coroutines
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$kotlinCoroutinesVersion")

                // kotlin serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:$kotlinSerializationVersion")

                // ktor
                implementation("io.ktor:ktor-client-ios:$ktorVersion")
                implementation("io.ktor:ktor-client-json-native:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization-native:$ktorVersion")

                // sqlDelight
                implementation("com.squareup.sqldelight:native-driver:$sqlDelightVersion")
            }
        }
        val iosTest by getting {

        }
    }
}
android {
    compileSdkVersion(29)
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
}

val packForXcode by tasks.creating(Sync::class) {
    group = "xcode"
    val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
    val framework = kotlin.targets.getByName<KotlinNativeTarget>("ios").binaries.getFramework(mode)
    inputs.property("mode", mode)
    dependsOn(framework.linkTask)
    val targetDir = File(buildDir, "xcode-frameworks")
    from({ framework.outputDirectory })
    into(targetDir)
}
tasks.getByName("build").dependsOn(packForXcode)