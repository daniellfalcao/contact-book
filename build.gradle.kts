buildscript {

    val kotlinVersion: String by project
    val gradleAndroidVersion: String by project
    val sqlDelightVersion: String by project

    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        maven {
            url = uri("https://dl.bintray.com/kotlin/kotlin-eap")
        }
        maven { url = uri("https://dl.bintray.com/icerockdev/moko") }
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath("org.jetbrains.kotlin:kotlin-serialization:$kotlinVersion")
        classpath("com.android.tools.build:gradle:$gradleAndroidVersion")
        classpath("com.squareup.sqldelight:gradle-plugin:$sqlDelightVersion")
    }
}

allprojects {
    repositories {
        maven { url = uri("https://dl.bintray.com/icerockdev/moko") }
    }
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlin-eap")
    }
    maven { url = uri("https://dl.bintray.com/icerockdev/moko") }
}

group = "br.turing.parrot"
version = "1.0.0"