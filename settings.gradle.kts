rootProject.name = "parrot"

pluginManagement {

    val gradleAndroidVersion: String by extra

    repositories {
        mavenCentral()
        gradlePluginPortal()
        google()
        jcenter()
        maven {
            url = uri("https://dl.bintray.com/kotlin/kotlin-eap")
        }
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.namespace == "com.android" || requested.id.name == "kotlin-android-extensions") {
                useModule("com.android.tools.build:gradle:$gradleAndroidVersion")
            }
        }
    }
}

include(":shared")
include(":android")

enableFeaturePreview("GRADLE_METADATA")

