package com.turingx.parrot.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.turingx.parrot.R
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.data.contact.ui.Contact.Companion.CONTACT

class ContactActivity: AppCompatActivity() {

    companion object {

        fun detailContact(context: Context, contact: Contact) {
            Intent(context, ContactActivity::class.java).run {
                putExtra(CONTACT, Contact.toJson(contact))
                context.startActivity(this)
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)
    }

}