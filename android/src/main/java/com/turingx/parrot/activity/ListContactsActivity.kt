package com.turingx.parrot.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.turingx.parrot.R
import com.turingx.parrot.adapter.ContactsAdapter
import com.turingx.parrot.core.usecase.CBUseCase
import com.turingx.parrot.viewmodel.ContactsViewModel
import kotlinx.android.synthetic.main.activity_list_contact.*

class ListContactsActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProvider(this).get(ContactsViewModel::class.java) }
    private val contactsAdapter by lazy { ContactsAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_contact)
        setupUI()
        subscribeUI()

        viewModel.loadContacts()
    }

    private fun setupUI() {
        setSupportActionBar(toolbar)
        recyclerView.adapter = contactsAdapter
        add.setOnClickListener {
            Intent(this, ContactActivity::class.java).run { startActivity(this) }
        }
    }

    private fun subscribeUI() = viewModel.run {

        contacts.addObserver {
            contactsAdapter.update(it)
        }

        viewState.addObserver { state ->

            empty_container.visibility = View.GONE
            loading_container.visibility = View.GONE
            error_container.visibility = View.GONE

            when (state) {
                is CBUseCase.Completable.State.Idle -> {
                    // ??
                }
                is CBUseCase.Completable.State.Executing -> {
                    if (contactsAdapter.isEmpty()) loading_container.visibility = View.VISIBLE
                }
                is CBUseCase.Completable.State.Success -> {
                    // ??
                }
                is CBUseCase.Completable.State.Error.Connection,
                is CBUseCase.Completable.State.Error.Generic -> {
                    if (contactsAdapter.isEmpty()) error_container.visibility = View.VISIBLE
                }
            }
        }

        onContactClicked.addObserver {
            it?.getContentIfNotHandled()?.let { contact ->
                ContactActivity.detailContact(this@ListContactsActivity, contact)
            }
        }
    }
}