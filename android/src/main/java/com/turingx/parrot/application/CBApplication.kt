package com.turingx.parrot.application

import android.app.Application
import com.turingx.parrot.core.database.CBDatabase

class CBApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        CBDatabase.initialize(this)
    }
}