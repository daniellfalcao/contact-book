package com.turingx.parrot.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.data.contact.ui.ContactHeader
import com.turingx.parrot.data.contact.ui.IContact
import com.turingx.parrot.viewholder.ContactHeaderViewHolder
import com.turingx.parrot.viewholder.ContactViewHolder
import com.turingx.parrot.viewholder.IContactViewHolder

class ContactsAdapter : RecyclerView.Adapter<IContactViewHolder>() {

    companion object {
        const val HEADER = 0
        const val CONTACT = 1
    }

    private val items = mutableListOf<IContact>()

    fun update(contacts: List<Contact>) {
        items.clear()
        contacts.groupBy { it.name[0].toString() }.forEach { (key, contacts) ->
            items.add(ContactHeader(key))
            items.addAll(contacts)
        }
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    fun isEmpty() = items.size == 0

    override fun getItemViewType(position: Int): Int {
        return if (items[position].isHeader()) HEADER else CONTACT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IContactViewHolder {
        return if (viewType == CONTACT) {
            ContactViewHolder.build(parent)
        } else {
            ContactHeaderViewHolder.build(parent)
        }
    }

    override fun onBindViewHolder(holder: IContactViewHolder, position: Int) {
        try {
            holder.bind(items[position])
        } catch (e: ArrayIndexOutOfBoundsException) {
        }
    }


}