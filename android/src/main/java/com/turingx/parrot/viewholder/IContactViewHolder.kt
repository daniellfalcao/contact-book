package com.turingx.parrot.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.turingx.parrot.data.contact.ui.IContact

abstract class IContactViewHolder(view: View): RecyclerView.ViewHolder(view) {
    abstract fun bind(contact: IContact)
}