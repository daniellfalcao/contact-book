package com.turingx.parrot.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.turingx.parrot.R
import com.turingx.parrot.data.contact.ui.ContactHeader
import com.turingx.parrot.data.contact.ui.IContact
import kotlinx.android.synthetic.main.view_holder_header_contact.view.*

class ContactHeaderViewHolder(view: View) : IContactViewHolder(view) {

    companion object {

        fun build(view: ViewGroup): ContactHeaderViewHolder {
            return ContactHeaderViewHolder(LayoutInflater.from(view.context).inflate(R.layout.view_holder_header_contact, view, false))
        }
    }

    override fun bind(contact: IContact) = itemView.run {
        if (contact is ContactHeader) initial_letter.text = contact.letter
    }

}