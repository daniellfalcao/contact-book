package com.turingx.parrot.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.turingx.parrot.R
import com.turingx.parrot.data.contact.ui.Contact
import com.turingx.parrot.data.contact.ui.IContact
import kotlinx.android.synthetic.main.view_holder_contact.view.*

class ContactViewHolder(view: View) : IContactViewHolder(view) {

    companion object {

        fun build(view: ViewGroup): ContactViewHolder {
            return ContactViewHolder(LayoutInflater.from(view.context).inflate(R.layout.view_holder_contact, view, false))
        }
    }

    override fun bind(contact: IContact) = itemView.run {
        if (contact is Contact) {
            initial_letter.text = contact.name[0].toString()
            name.text = contact.name
        }
    }
}